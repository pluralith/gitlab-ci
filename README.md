![GitHub Badge Blue](https://user-images.githubusercontent.com/25454503/158019834-99b2365e-94c4-4139-80ef-3e0b7bf354f0.svg)
# Pluralith Gitlab CI Template

Run Pluralith in Gitlab CI to automate your infrastructure documentation and save your team hours of busy-work. Pluralith posts a diagram of your infrastructure as a merge request comment. It also keeps a history of all changes made to your infrastructure which you can access at any time.

Pluralith diagrams also aggregate a variety of relevant data into the visual:
- Change Highlighting
- Drift Detection & Highlighting
- Cost Data (via Infracost)
- Compliance Data (coming soon)


### 📍 The result looks like this:

See [this](https://gitlab.com/pluralith/gitlab-ci/-/merge_requests/3) merge request for a real life example
![Gitlab CI Example](https://user-images.githubusercontent.com/25454503/176441201-c342f801-8edc-414b-9471-626b21e6e806.png)


## ⚙️ Getting Started

Follow these steps to get Pluralith running in your Gitlab CI workflow:
1. Set `PLURALITH_API_KEY` and `PLURALITH_PROJECT_ID` as [CI/CD variables](https://docs.gitlab.com/ee/ci/variables/). You can get your API Key and Project ID through the Pluralith Dashboard.
2. Set credentials for the providers of your choice as CI/CD variables *(e.g. for AWS set `AWS_ACCESS_KEY` and `AWS_SECRET_KEY`)*.
3. Set a `GITLAB_TOKEN` as a CI/CD variable so Pluralith can post merge request comments. (Requires the `api` scope enabled to be able to read and write comments on merge requests)
4. Create a new `.gitlab-ci.yml` file in your repo and copy the below example into it:

```yaml
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'

image:
    name: node:latest
    entrypoint: [""]

.setup-terraform: &setup-terraform
    - apt-get update && apt-get install -y gnupg software-properties-common curl
    - curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add -
    - apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
    - apt-get update && apt-get install terraform
    - cd $TF_ROOT
    - terraform init

.setup-pluralith: &setup-pluralith
    - curl https://raw.githubusercontent.com/Pluralith/pluralith-cli/main/scripts/install.sh | sh

.init: &init
    - pluralith init --api-key=$PLURALITH_API_KEY --project-id=$PLURALITH_PROJECT_ID

.run: &run
    - pluralith run --title=$TITLE --version=$VERSION --show-changes=$SHOW_CHANGES --show-drift=$SHOW_DRIFT --show-costs=$SHOW_COSTS --cost-mode=$COST_MODE --cost-period=$COST_PERIOD --export-pdf=$EXPORT_PDF
           
.comment: &comment
    - compost autodetect update --body-file=comment.md

Pluralith:
    script:
        - *setup-terraform
        - *setup-pluralith
        - *init
        - *run
        - *comment
    variables:
        # - - Terraform - - 
        TF_ROOT: terraform/application                  # Your Terraform project directory
        TF_VERSON: 1.0.0                                # Your preferred Terraform version (Note that Pluralith requires at least v0.15.3)
        # - - Credentials - -
        PLURALITH_API_KEY: $PLURALITH_API_KEY           # Your Pluralith API key
        PLURALITH_PROJECT_ID: $PLURALITH_PROJECT_ID     # ID of the project this run should be posted to
        GITLAB_TOKEN: $GITLAB_TOKEN                     # For merge request comments the 'api' scope is required
        # Set credentials for your cloud provider below (Example: AWS)
        AWS_ACCESS_KEY_ID: $PLR_AWS_ACCESS_KEY
        AWS_SECRET_ACCESS_KEY: $PLR_AWS_SECRET_KEY
        # - - Diagram - -
        # Set your diagram details below, these will determine how the final diagram looks
        TITLE: "Gitlab Demo" # Only relevant if EXPORT_PDF is true -> Custom title to be used for locally generated PDF
        VERSION: "0.0.1" # Only relevant if EXPORT_PDF is true -> Add version string to locally generated PDF
        SHOW_CHANGES: "false"
        SHOW_DRIFT: "false"
        SHOW_COSTS: "false" # Only relevant if infracost is set up
        COST_MODE: "delta" # Only relevant if infracost is set up
        COST_PERIOD: "hour" # Only relevant if infracost is set up
        EXPORT_PDF: "false"
```

5. That's it! Create a merge request and see the magic happen in its comments!

&nbsp;

## 👩‍🚀 Looking to become a tester or talk about the project?
- Sign up for the `alpha` over on our **[Website](https://www.pluralith.com)**
- Join our **[Subreddit](https://www.reddit.com/r/Pluralith/)**
- Check out our **[Roadmap](https://roadmap.pluralith.com)** and upvote features you'd like to see
- Or just shoot us a message on Linkedin:
  -  [Dan's Linkedin](https://www.linkedin.com/in/danielputzer/)
  -  [Phi's Linkedin](https://www.linkedin.com/in/philipp-weber-a8517b231/)

*Disclaimer: To properly use Pluralith in Gitlab CI you **will need an API key**. [Sign up](https://www.pluralith.com) for the private alpha to get access!*

![Subreddit subscribers](https://img.shields.io/reddit/subreddit-subscribers/pluralith?style=social)
