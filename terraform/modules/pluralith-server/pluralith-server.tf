
# - - Pluralith Server Infrastructure - -

# - -
# - - Network Parameter Variables - -
# - -

variable "PluralithVPC" {
    type = string
}

variable "PluralithPrivateSubnets" {
    type = list(string)
}

variable "PluralithPublicSubnets" {
    type = list(string)
}


# - -
# - - Server Resource Creation - -
# - -

# - - Security Groups - -
resource "aws_security_group" "PluralithLoadBalancerSG" {
    name = "LoadBalancerPorts"
    vpc_id = var.PluralithVPC

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    tags = {
        Name = "PluralithLoadBalancerSG"
    }
}

resource "aws_security_group" "PluralithWebServerSG" {
    name = "WebServerPorts"
    vpc_id = var.PluralithVPC

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    ingress {
        from_port = 80
        to_port = 80
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    ingress {
        from_port = 8080
        to_port = 8080
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    tags = {
        Name = "PluralithWebServerSG"
    }
}

resource "aws_security_group" "PluralithJumpBoxSG" {
    name = "JumpBoxPorts"
    vpc_id = var.PluralithVPC

    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    egress {
        from_port = 0
        to_port = 65535
        protocol = "tcp"
        cidr_blocks = [ "0.0.0.0/0" ]
    }

    tags = {
        Name = "PluralithJumpBoxSG"
    }
}


# - - Launch Configuration - -
resource "aws_launch_configuration" "PluralithLaunchConfig" {
    image_id = "ami-0e1ce3e0deb8896d2"
    instance_type = "t2.micro"
    key_name = "ServerSSH"
    security_groups = [ aws_security_group.PluralithWebServerSG.id ]
    user_data = file("./ec2init.sh")

    ebs_block_device {
        device_name = "/dev/sdk"
        volume_size = 10
    }

    lifecycle {
        create_before_destroy = true
    }
}

# - - AutoScaling Group - -
resource "aws_autoscaling_group" "PluralithAutoScaling" {
    launch_configuration = aws_launch_configuration.PluralithLaunchConfig.name
    vpc_zone_identifier = var.PluralithPrivateSubnets
    min_size = 2
    max_size = 4
    target_group_arns = [ aws_lb_target_group.PluralithApacheTargetGroup.id, aws_lb_target_group.PluralithFlaskTargetGroup.id ]
}

# - - Load Balancer Target Groups - -
resource "aws_lb_target_group" "PluralithApacheTargetGroup" {
    name = "PluralithApacheTargetGroup"
    vpc_id = var.PluralithVPC
    port = 80
    protocol = "HTTP"
    
    health_check {
        protocol = "HTTP"
        port = 80
        path = "/"
        interval = 35
        timeout = 30
        healthy_threshold = 2
        unhealthy_threshold = 5
    }
}

resource "aws_lb_target_group" "PluralithFlaskTargetGroup" {
    name = "PluralithFlaskTargetGroup"
    vpc_id = var.PluralithVPC
    port = 8080
    protocol = "HTTP"
    
    health_check {
        protocol = "HTTP"
        port = 8080
        path = "/"
        interval = 35
        timeout = 30
        healthy_threshold = 2
        unhealthy_threshold = 5
    }
}

# - - Load Balancer + Listener - -
resource "aws_lb" "PluralithLoadBalancer" {
    name = "PluralithLoadBalancer"
    security_groups = [ aws_security_group.PluralithLoadBalancerSG.id ]
    subnets = var.PluralithPublicSubnets
    load_balancer_type = "application"

    tags = {
        name = "PluralithLoadBalancer"
    }
}

resource "aws_lb_listener" "PluralithApacheListener" {
    load_balancer_arn = aws_lb.PluralithLoadBalancer.arn
    port = 80
    protocol = "HTTP"

    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.PluralithApacheTargetGroup.id
    }
}

resource "aws_lb_listener" "PluralithFlaskListener" {
    load_balancer_arn = aws_lb.PluralithLoadBalancer.arn
    port = 8080
    protocol = "HTTP"

    default_action {
        type = "forward"
        target_group_arn = aws_lb_target_group.PluralithFlaskTargetGroup.id
    }
}

# - - JumpBox - -
resource "aws_instance" "PluralithJumpBox" {
    ami = "ami-0e1ce3e0deb8896d2"
    instance_type = "t2.micro"
    associate_public_ip_address = true
    subnet_id = var.PluralithPublicSubnets[0]
    security_groups = [ aws_security_group.PluralithJumpBoxSG.id ]
    key_name = "JumpBoxSSH"
    user_data = file("./ec2init.sh")

    tags = {
        Name = "PluralithJumpBox"
    }
}