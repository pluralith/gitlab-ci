# - - Intializing Terraform - -

terraform {
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }
}

# Configuring AWS provider
provider "aws" {
    region = "eu-central-1"
}