
# - -
# - - Pluralith Infrastructure - -
# - -


# - - Importing Modules - -

module "PluralithNetwork" {
    source = "../modules/pluralith-network"
}

module "PluralithServer" {
    source = "../modules/pluralith-server"
    depends_on = [ module.PluralithNetwork ]
    PluralithVPC = module.PluralithNetwork.PluralithVPC
    PluralithPrivateSubnets = module.PluralithNetwork.PluralithPrivateSubnets
    PluralithPublicSubnets = module.PluralithNetwork.PluralithPublicSubnets
}