#!/bin/bash
apt-get update -y
apt-get install apache2 -y
systemctl start apache2.service
apt-get install python3 -y
apt-get update -y
apt-get install python3-venv -y
mkdir sentiment-api
git clone https://github.com/DanThePutzer/demo-apps.git
chown -R ubuntu /var/www/html
mv demo-apps/demo-nlp/* sentiment-api/
mv demo-apps/demo-web/* /var/www/html/
python3 -m venv pluralith-env
source pluralith-env/bin/activate
cd sentiment-api
pip install -r requirements.txt
flask run --host=0.0.0.0 --port=8080
